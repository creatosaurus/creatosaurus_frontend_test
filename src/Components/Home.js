import React, { useState } from 'react'
import Axios from 'axios'
import './Home.css'

const Home = (props) => {

    const [email, setemail] = useState("")
    const [password, setpassword] = useState("")

    const Login = (e) => {
        e.preventDefault()
        Axios.post('user/login', {
            email: email,
            password: password
        }).then(res => {
            console.log(res)
            props.history.push('/product',{
                state:res.data
            })
        }).catch(err => {
            alert(err.response.data.message)
        })
    }

    return (
        <div className="login-container">
            <div className="login-card">
                <form className="login-form" onSubmit={(e) => e.preventDefault()}>
                    <div style={{ marginBottom: 20, display: 'flex', flexDirection: 'column' }}>
                        <label>Email:</label>
                        <input style={{ width: '100%', height: 10, padding: 10, borderRadius: 10 }} id="email" type="text" onChange={(e) => setemail(e.target.value)}></input>
                    </div>
                    <div style={{ marginBottom: 20, display: 'flex', flexDirection: 'column' }}>
                        <label>Password:</label>
                        <input style={{ width: '100%', height: 10, padding: 10, borderRadius: 10 }} id="pass" type="password" onChange={(e) => setpassword(e.target.value)} ></input>
                    </div>
                    <div>
                        <button onClick={(e) => Login(e)}>Login</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Home
