import React from 'react'
import './Product.css'

const Product = (props) => {
    const { id } = props.location.state.state
    return (
        <div className="product-container">
            <div className="product-card-container">
                <a href={`http://yogved.in/apollo/${id}`} >Apollo</a>
                <a href={`http://yogved.in/cache/${id}`}>Cache</a>
                <a href={`http://yogved.in/muse/home`}>Muse</a>
                <a href={`http://yogved.in/localstory/${id}`}>Local Story</a>
                <a href={`http://yogved.in/${id}`}>Cloud Storage</a>
            </div>
        </div>
    )
}

export default Product
