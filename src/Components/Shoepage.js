import React from 'react'

const Shoepage = () => {
    return (
        <div
            style={{
                height: '100vh', backgroundColor: "#dcdcdc", display: 'flex',
                justifyContent: 'center', alignItems: 'center',
                fontSize:50,
                fontWeight:'bold'
            }}>
            We will be back soon...
        </div>
    )
}

export default Shoepage
